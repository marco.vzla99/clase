import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/caracteristicas/bloc.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/registro_usuario.dart';

class VistaMostrandoNombreNoConfirmado extends StatelessWidget {
  final NickFormado nick;
  const VistaMostrandoNombreNoConfirmado(this.nick,{super.key});

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Center(
      child: Column(children: [
        Text('Este fulano ${nick.valor} no esta en BoardGameGeek'),
        TextButton(onPressed: () {
            elBloc.add(Creado());
          },
          child: const Text('Volver'))
      ]));        
  }
}