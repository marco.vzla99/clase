import 'package:flutter/material.dart';
import 'package:proyecto/caracteristicas/bloc.dart';
import 'package:proyecto/dominio/registro_usuario.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/dominio/nick_formado.dart';

class VistaMostrandoNombreConfirmado extends StatelessWidget {
  final RegistroUsuario registro;
  final NickFormado nick;
  const VistaMostrandoNombreConfirmado(this.registro,this.nick,{super.key});

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Column(
      children: [
        Text('${registro.apellidos} ${registro.nombre}'),
        Text('${registro.anioRegistro} '),
        Text('${registro.estado} ${registro.pais}'),
          TextButton(onPressed: () {
            elBloc.add(Creado());
          },
          child: const Text('Volver')),
          TextButton(onPressed: (){
            elBloc.add(JuegosConfirmados(nick));
          }, child: const Text('Juegos jugados'))
      ],
    );
  }
}