import 'package:flutter/material.dart';

class VistaMostrandoJuegosJugados extends StatelessWidget {
  final List<String> juegosJugados;
  const VistaMostrandoJuegosJugados(this.juegosJugados,{super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: juegosJugados.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(juegosJugados[index], textAlign: TextAlign.center ),
      );
      },
    );    
  }
}