import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/caracteristicas/bloc.dart';

class VistaMostrandoSolicitudActualizacion extends StatelessWidget {
  const VistaMostrandoSolicitudActualizacion({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final b = context.read<BlocVerificacion>();
    return Center(
      child: Column(
        children: [
          // ignore: prefer_const_constructors
          Text(
            'Actualizate compadre',
          ),
          TextButton(onPressed: (){
            b.add(Creado());
          }, child: const Text('Volver'))
        ],
      ),
    );
  }
}