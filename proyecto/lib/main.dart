// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:proyecto/caracteristicas/bloc.dart';
import 'package:proyecto/dominio/repositorio_verificacion.dart';
import 'package:proyecto/vistas/nombre_confirmado.dart';
import 'package:proyecto/vistas/nombre_no_confirmado.dart';
import 'package:proyecto/vistas/vista_actualizacion.dart';
import 'package:proyecto/vistas/vista_juegos_jugados.dart';
import 'package:proyecto/vistas/vistas_creandose.dart';
import 'package:proyecto/vistas/vista_esperando_nombre.dart';
void main() {
 runApp(const AplicacionInyectada());
}

class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context){
        BlocVerificacion blocVerificacion =BlocVerificacion(RepositorioReal());
        Future.delayed(Duration(seconds: 2),(){
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}
class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
       body: Builder(builder: (context){
         var estado = context.watch<BlocVerificacion>().state;
         if (estado is Creandose){
           return Center (child: const VistaCreandose());
         }
         if (estado is SolicitandoNombre){
           return const VistaSolicitandoNombre();
         }
         if (estado is MostrandoSolicitudActualizacion){
          return const VistaMostrandoSolicitudActualizacion();
         }
         if (estado is MostrandoNombreNoConfirmado){
          return VistaMostrandoNombreNoConfirmado(estado.nick);
         }
         if (estado is MostrandoNombreConfirmado){
          return VistaMostrandoNombreConfirmado(estado.registroUsuario, estado.nick);
         }
         if (estado is MostrandoJuegosJugados) {
           return VistaMostrandoJuegosJugados(estado.juegosJugados);
         }
         return const Center(
            child: Text('Si estas viendo esto, algo salio mal, HUYE')); 
       },), 
      ),
    );
  }
}