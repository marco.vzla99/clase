class RepositorioJugadas{
  final String _jugadas ="""plays username="benthor" userid="597373" total="1737" page="1" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
  <play id="34017961" date="2019-02-21" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="The Dwarf King" objecttype="thing" objectid="85250">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play>
  <play id="34017955" date="2019-02-21" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="Takenoko" objecttype="thing" objectid="70919">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play>
  <play id="34004213" date="2019-02-13" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="RoboRally" objecttype="thing" objectid="18">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play>
  <play id="34004226" date="2019-02-13" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="Takenoko" objecttype="thing" objectid="70919">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play>
  <play id="34004202" date="2019-02-12" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="Splendor" objecttype="thing" objectid="148228">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play>
  <play id="34004193" date="2019-02-07" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="Just One" objecttype="thing" objectid="254640">
      <subtypes>
        <subtype value="boardgame"/>
        <subtype value="boardgameimplementation"/>
      </subtypes>
    </item>
  </play>
  <play id="34004135" date="2019-02-07" quantity="1" length="0" incomplete="0" nowinstats="0" location="">
    <item name="Tiny Epic Galaxies" objecttype="thing" objectid="163967">
      <subtypes>
        <subtype value="boardgame"/>
      </subtypes>
    </item>
  </play> 
</plays>""";
  
  static const campoUsername = 'username';
  static const campoUserId = 'userId';
  static const campoTotal= 'total';
  static const campoPage= 'page';
  static const campoJugadaId= 'id';
  static const campoFecha= 'date';
  static const campoCantidad= 'quantity';
  static const campoLongitud= 'length';
  static const campoIncompleto= 'incomplete';
  static const campoDerrota= 'nowinstats';
  static const campoUbicacion= 'location';
  static const campoNombreJuego= 'name';
  static const campoTipoObjeto= 'objecttype';
  static const campoObjetoId= 'objectid';
  static const campoSubTipo= 'subtype';
}