import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/problemas.dart';
import 'package:xml/xml.dart';
import 'package:http/http.dart' as http;

abstract class RepositorioXml{
  Future<Either <Problema, List<String>>> obtenerXml(NickFormado nick);
}

class RepositorioXmlReal extends RepositorioXml{
  @override
  Future<Either<Problema,List<String>>> obtenerXml(NickFormado nick) async{
    try {
      Uri direccion = Uri.http('www.boardgamegeek.com','xmlapi2/plays',{'username':nick.valor});
      var respuesta = await http.get(direccion);
      int paginasXml = _obtenerCuantasPaginasDesdeXml(respuesta.body);
      List<String> arrayJuegos = await obtenerXmls(paginasXml, nick);
      return Right(arrayJuegos);
    } catch (e) {
      return Left(ServidorNoAlcanzado());
    }
  }

  Future<List<String>> obtenerXmls(int paginasXml, NickFormado nick) async {
    List<String> arrayJuegos = [];
    for(int i = 0; i < paginasXml; i++){
      Uri direccion = Uri.http('www.boargamegeek.com', 'xmlapi2/plays',{'username':nick.valor});
      final respuesta = await http.get(direccion);
      if(respuesta.statusCode == 200){
        arrayJuegos.add (respuesta.body);
      }
    }
    return arrayJuegos;
  }
}
class RepositorioXmlPruebas extends RepositorioXml{
  @override
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick) async{  
    if (nick.valor == 'Benthor'){
      try {
        String elXml = File ('./test/caracteristicas/juegos_jugados/benthor1.xml').readAsStringSync();
        int cuantasPaginas = _obtenerCuantasPaginasDesdeXml(elXml);
        List<String> nombresPaginas =_obtenerNombresPaginas(cuantasPaginas, nick);
        return  Right(nombresPaginas.map((e) => File(e).readAsStringSync()).toList());
      } catch (e) {
        return Left(VersionIncorrectaXml());
      }
    }

    if (nick.valor == 'Fokuleh'){
      try {
        String elXml = File ('./test/caracteristicas/juegos_jugados/fokuleh1.xml').readAsStringSync();
        int cuantasPaginas = _obtenerCuantasPaginasDesdeXml(elXml);
        List<String> nombresPaginas =_obtenerNombresPaginas(cuantasPaginas, nick);
        return  Right(nombresPaginas.map((e) => File(e).readAsStringSync()).toList());
      } catch (e) {
        return Left(VersionIncorrectaXml());
      }
    }
    return Left(UsuarioNoRegistrado());    
  }
}

const tamanioPagina = 100;  
int _obtenerCuantasPaginasDesdeXml(String elXml){
  XmlDocument documento = XmlDocument.parse(elXml);
  String cadenaNumero = documento.getElement('plays')!.getAttribute('total')!;
  int numero =int.parse(cadenaNumero);
  return (numero / tamanioPagina).ceil();
}

List<String> _obtenerNombresPaginas(cuantasPaginas, NickFormado nick){
    final base = './test/caracteristicas/juegos_jugados/${nick.valor}';
    List<String> lista = [];
    for (int i = 1; i<=cuantasPaginas; i++){
      lista.add('$base$i.xml');
    }
    return lista;
}

