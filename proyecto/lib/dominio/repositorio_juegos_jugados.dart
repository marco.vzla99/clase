import 'dart:io';
import 'package:fpdart/fpdart.dart';
import 'package:proyecto/dominio/juego_jugado.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/problemas.dart';
import 'package:proyecto/dominio/repositorio_xml.dart';
import 'package:xml/xml.dart';

abstract class RepositorioJuegosJugados {
  final RepositorioXml repositorio;
  RepositorioJuegosJugados(this.repositorio);
  Future<Either<Problema, Set<JuegoJugado>>> obtenerJuegosJugados(
      NickFormado nick);
}

class RepositorioJuegosJugadosPruebas extends RepositorioJuegosJugados {
  RepositorioJuegosJugadosPruebas (repositorio):super(repositorio);
  @override
  Future<Either<Problema, Set<JuegoJugado>>> obtenerJuegosJugados(NickFormado nick) async {
    final Either<Problema,List<String>> resultadoXml = await super.repositorio.obtenerXml(nick);
    return resultadoXml.match((l){
      return Left(l);
    }, (r) {
      final resultado = _obtenerJuegosJugadosDesdeXml(r, nick);
      return resultado;
    });    
  }
}

Either<Problema, Set<JuegoJugado>> _obtenerJuegosJugadosDesdeXml(List<String> elXml, NickFormado nick) {
  final resultado = elXml.map((e) => _obtenerUnSoloSet(e));
  Set<JuegoJugado> union = {};
  if (resultado.any((element) => element is Problema)){
    return Left(VersionIncorrectaXml());
  }      
  final soloSets = resultado.map((e) => e.getOrElse((l) => {}));
  union = soloSets.fold<Set<JuegoJugado>>({}, (p, a) => a..addAll(p.toList()));
  _crearListaJuegosJugados(union, nick);
  return Right(union);
}

Either<Problema, Set<JuegoJugado>> _obtenerUnSoloSet(String elXml){
  try {
    XmlDocument documento = XmlDocument.parse(elXml);
    final juegos = documento.findAllElements('item');
    final conjuntoIterable = juegos.map((e) {
      String nombre = e.getAttribute('name')!;
      String id = e.getAttribute('objectId')!;
      return JuegoJugado.constructor(nombrePropuesto: nombre, idPropuesto: id);
    });
    final conjunto = Set<JuegoJugado>.from(conjuntoIterable);
    return Right (conjunto);
  } catch (e) {
    return Left(VersionIncorrectaXml());
  }
}

void _crearListaJuegosJugados(Set<JuegoJugado> union, NickFormado nick){
  var juego = <String>[];
  for (var e in union) {
    juego.add("${e.id} - ${e.nombre}");
  }
  File('./test/caracteristicas/juegos_jugados/juegos${nick.valor}.xml').writeAsString(juego.toString());
}