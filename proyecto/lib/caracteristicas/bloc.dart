import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/problemas.dart';
import 'package:proyecto/dominio/registro_usuario.dart';
import 'package:proyecto/dominio/repositorio_verificacion.dart';

class EventoVerificacion { }
class Creado extends EventoVerificacion{ }
class NombreRecibido extends EventoVerificacion{
  final NickFormado nick;
  NombreRecibido(this.nick);
 }
class NombreConfirmado extends EventoVerificacion{ }
class JuegosConfirmados extends EventoVerificacion {
  final NickFormado nick;
  JuegosConfirmados(this.nick);
}

class EstadoVerificacion{ }
class Creandose extends EstadoVerificacion{ }
class SolicitandoNombre extends EstadoVerificacion{ }
class EspertandoConfirmacionNombre extends EstadoVerificacion{ }
class MostrandoSolicitudActualizacion extends EstadoVerificacion {}

class MostrandoJuegosJugados extends EstadoVerificacion {
  final List<String> juegosJugados;
  MostrandoJuegosJugados(this.juegosJugados);
}

class MostrandoNombreNoConfirmado extends EstadoVerificacion{
  final NickFormado nick;
  MostrandoNombreNoConfirmado(this.nick);
}

class MostrandoNombreConfirmado extends EstadoVerificacion{
  final NickFormado nick;
  final RegistroUsuario registroUsuario;
  MostrandoNombreConfirmado(this.registroUsuario,this.nick);
}
class MostrarNombre extends EstadoVerificacion{ }

class BlocVerificacion extends Bloc<EventoVerificacion, EstadoVerificacion> {
  final RepositorioVerificacion _repositorioVerificacion;
  BlocVerificacion(this._repositorioVerificacion) : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoNombre());
    });

    on<NombreRecibido>((event, emit) async {
      emit (EspertandoConfirmacionNombre());
      final resultado = await _repositorioVerificacion.obtenerRegistroUsuario(event.nick);
      resultado.match((l){
        if (l is VersionIncorrectaXml){
          emit(MostrandoSolicitudActualizacion());
        }
        if(l is UsuarioNoRegistrado){
          emit(MostrandoNombreNoConfirmado(event.nick));
        }
      }, (r){
        emit(MostrandoNombreConfirmado(r,event.nick));
      });
    });
    on<JuegosConfirmados>((event, emit) {
      List<String> paginas = File('./test/caracteristicas/juegos_jugados/juegos${event.nick.valor}.xml').readAsStringSync().split(', ');
      emit(MostrandoJuegosJugados(paginas));
    });
  }
}