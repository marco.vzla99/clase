import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/problemas.dart';
import 'package:proyecto/dominio/registro_usuario.dart';
import 'package:proyecto/dominio/repositorio_verificacion.dart';

void main() {
  group('Pruebas verificacion offline', () {
    test('Con benthor me regresa valor', () async {
      RepositorioReal repositorio = RepositorioReal ();
      var resultado = await repositorio.obtenerRegistroUsuario(NickFormado.constructor('benthor'));
      resultado.match(
        (l) {
          expect(false, equals(true));
        },((r) {
          expect(r.anioRegistro, equals(2012));
          expect(r.nombre, equals('Benthor'));
          expect(r.apellidos, equals('Benthor'));
          expect(r.estado, equals(mensajeCampoVacio));
          expect(r.pais, equals(mensajeCampoVacio));
        })); 
    });
    test('Con amlo me regresa error',() async{
      RepositorioPruebasVerificacion repositorio = 
        RepositorioPruebasVerificacion();

      var resultado = await repositorio.obtenerRegistroUsuario(NickFormado.constructor('amlo'));

      resultado . match((l){
        expect(true, equals(true));
      }, (r){
        expect(true, equals(false));
      });
    });

    test('Con XML incorrecto',() async {
      RepositorioPruebasVerificacion repositorio = 
        RepositorioPruebasVerificacion();

      var resultado = await repositorio.obtenerRegistroUsuario(NickFormado.constructor('incorrecto'));

      resultado . match((l){
        expect(l, isA<VersionIncorrectaXml>());
      }, (r){
        assert(false);
      });
    });

  });

}