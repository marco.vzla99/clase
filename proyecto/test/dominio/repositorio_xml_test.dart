import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/repositorio_xml.dart';
import 'package:test/test.dart';

void main() {
  test('Si le paso Benthor me debe regresar un solo XML', () async{
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado = await repositorio.obtenerXml(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(1));
    });
  });

  test('Si le paso Fokuleh me debe regresar 4 Xml', () async{
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado = await repositorio.obtenerXml(NickFormado.constructor('Fokuleh'));
    resultado.match((l) {
      expect(true, equals (false));
    }, (r) {
      expect(r.length, equals(4));
    });    
  });
}