import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto/dominio/registro_usuario.dart';


void main() {
  group('Registro usuario correcto', () {

    test('Prueba', () {
      final RegistroUsuario r = RegistroUsuario.constructor(
      propuestaAnio: "2012",
      propuestaNombre: 'x',
      propuestaApellidos: 's',
      propuestaPais: 'x',
      propuestaEstado: 'xc');
      expect(r.anioRegistro, equals(2012));
    });

  });    
  
}