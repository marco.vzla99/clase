import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto/dominio/juego_jugado.dart';
import 'package:proyecto/dominio/problemas.dart';

void main() {
  test('Mismo nombre diferente Id son diferentes', () {
    JuegoJugado j1 =
        JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
    JuegoJugado j2 =
        JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '2');
    expect(j1 == j2, equals(false));
  });
  test('Mismo nombre mismo Id son iguales', () {
    JuegoJugado j1 =
        JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
    JuegoJugado j2 =
        JuegoJugado.constructor(nombrePropuesto: 'uno', idPropuesto: '1');
    expect(j1 == j2, equals(true));
  });

  test('No se permite id vacio', () {
    expect(
        () => JuegoJugado.constructor(nombrePropuesto: 'j1', idPropuesto: ''),
        throwsA(isA<JuegoJugadoMalFormado>()));
  });
  test('No se permite nombre vacio', () {
    expect(() => JuegoJugado.constructor(nombrePropuesto: '', idPropuesto: ''),
        throwsA(isA<JuegoJugadoMalFormado>()));
  });
}
