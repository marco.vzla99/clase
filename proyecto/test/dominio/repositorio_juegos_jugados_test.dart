import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto/dominio/juego_jugado.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/repositorio_juegos_jugados.dart';
import 'package:proyecto/dominio/repositorio_xml.dart';

void main() {
  test('Benthor jugo 5 juegos', () async {
    RepositorioXmlPruebas repositorioXmlPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioXmlPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(resultado.length(), equals(5));
    });
  });

  test('Takenoko es de los jugados por Benthor', () async {
    RepositorioXmlPruebas repositorioXmlPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioXmlPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final takenoko = JuegoJugado.constructor(
        nombrePropuesto: 'Takenoko', idPropuesto: '70919');
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.contains(takenoko), equals(true));
    });
  });

  test('Monopoly no es de los jugados por Benthor', () async {
    RepositorioXmlPruebas repositorioXmlPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioXmlPruebas);
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final monopoly = JuegoJugado.constructor(nombrePropuesto: 'Monopoly', idPropuesto: '9');
    resultado.match((l) {
      assert(false);
    }, (r) {
      expect(r.contains(monopoly), equals(false));
    });
  });

  test('Abyss fue jugado por Fokuleh', () async {
    RepositorioXmlPruebas repositorioXmlPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio = RepositorioJuegosJugadosPruebas(repositorioXmlPruebas);
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor('Fokuleh'));
    final monopoly = JuegoJugado.constructor(nombrePropuesto: 'Abyss', idPropuesto: '155987');
    resultado.match((l) {
      assert(false);
    }, (r) {
      expect(r.contains(monopoly), equals(true));
    });
  });

  test('Monopoly no es de los jugados por Fokuleh', () async {
    RepositorioXmlPruebas repositorioXmlPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioXmlPruebas);
    final resultado = await repositorio.obtenerJuegosJugados(NickFormado.constructor('Fokuleh'));
    final monopoly = JuegoJugado.constructor(nombrePropuesto: 'Monopoly', idPropuesto: '9');
    resultado.match((l) {
      assert(false);
    }, (r) {
      expect(r.contains(monopoly), equals(true));
    });
  });

}
