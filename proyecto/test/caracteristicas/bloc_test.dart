import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto/caracteristicas/bloc.dart';
import 'package:proyecto/dominio/nick_formado.dart';
import 'package:proyecto/dominio/repositorio_verificacion.dart';
void main() {
  blocTest<BlocVerificacion, EstadoVerificacion>(
    'emits [MyState] when MyEvent is added.',
    build: () => BlocVerificacion(RepositorioPruebasVerificacion()),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoNombre>()],
  );
  blocTest<BlocVerificacion, EstadoVerificacion>(
    'Cuando NombreRecibido es Benthor, debe tener MostrandoNombreConfirmado',
    build: () => BlocVerificacion(RepositorioPruebasVerificacion()),
    seed: () => SolicitandoNombre(),
    act: (bloc) => bloc.add(NombreRecibido(NickFormado.constructor('benthor'))),
    expect: () => [isA<EspertandoConfirmacionNombre>(),
                   isA<MostrandoNombreConfirmado>()],
  );

  blocTest<BlocVerificacion, EstadoVerificacion>(
    'Cuando NombreRecibido es Amlo, debe tener MostrandoNombreConfirmado',
    build: () => BlocVerificacion(RepositorioPruebasVerificacion()),
    seed: () => SolicitandoNombre(),
    act: (bloc) => bloc.add(NombreRecibido(NickFormado.constructor('amlo'))),
    expect: () => [isA<EspertandoConfirmacionNombre>(),
                   isA<MostrandoNombreConfirmado>()],
  );

  blocTest<BlocVerificacion, EstadoVerificacion>(
    'Cuando NombreRecibido es incorrecto, debe tener MostrandoSolicitudActualizacion',
    build: () => BlocVerificacion(RepositorioPruebasVerificacion()),
    seed: () => SolicitandoNombre(),
    act: (bloc) => bloc.add(NombreRecibido(NickFormado.constructor('incorrecto'))),
    expect: () => [isA<EspertandoConfirmacionNombre>(),
                   isA<MostrandoSolicitudActualizacion>()],
  );
}